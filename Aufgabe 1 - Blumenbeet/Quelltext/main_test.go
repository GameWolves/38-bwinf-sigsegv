package main

import (
	"testing"
	"sync"
)

type Test struct {
	filename string
	output string
}


func TestFlowerbed(t *testing.T) {
	var wg sync.WaitGroup
	var tests []Test = []Test{
		{
			"beispieldaten/blumen.txt",
			"gelb gruen orange rosa rot tuerkis blau blau rot",
		},
		{
			"beispieldaten/blumen1.txt",
			"rot tuerkis tuerkis rot rot rot tuerkis tuerkis rot",
		},
		{
			"beispieldaten/blumen2.txt",
			"blau gruen rot rot rot gruen gruen rot rot",
		},
		{
			"beispieldaten/blumen3.txt",
			"blau gelb orange rosa rot gruen tuerkis tuerkis rot",
		},
		{
			"beispieldaten/blumen4.txt",
			"gelb orange rosa gruen rot blau tuerkis rosa rot",
		},
		{
			"beispieldaten/blumen5.txt",
			"gelb orange rosa gruen rot blau tuerkis tuerkis rot",
		},
	}
	for _, i := range tests {
		wg.Add(1)
		go func(test Test) {
			result := findBestFlowerbed(parseFile(test.filename))
			defer wg.Done()
			if result.bed.String() != test.output {
				t.Errorf("Expected {%v}, got {%v}", test.output, result.bed.String())
			}
		}(i)
	}
	wg.Wait()
}

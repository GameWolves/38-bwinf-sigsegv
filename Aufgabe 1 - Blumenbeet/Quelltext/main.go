package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"bufio"
	"strconv"
	"time"
)

var availableColors []string = []string{"blau", "gelb", "gruen", "orange", "rosa", "rot", "tuerkis"}

type preferences struct {
	colors int
	combinations []combination
}

type combination struct {
	color1 string
	color2 string
	points int
}

type flower struct {
	color string
	neighbours []int
}

type flowerbed struct {
	flowers [9]flower
}

func createFlowerbed() flowerbed {
	var bed = flowerbed{}
	bed.flowers[0].neighbours = []int{1, 2}
	bed.flowers[1].neighbours = []int{0, 2, 3, 4}
	bed.flowers[2].neighbours = []int{0, 1, 4, 5}
	bed.flowers[3].neighbours = []int{1, 4, 6}
	bed.flowers[4].neighbours = []int{1, 2, 3, 5, 6, 7}
	bed.flowers[5].neighbours = []int{2, 4, 7}
	bed.flowers[6].neighbours = []int{3, 4, 7, 8}
	bed.flowers[7].neighbours = []int{5, 4, 6, 8}
	bed.flowers[8].neighbours = []int{6, 7}
	return bed
}

func (c combination) Fits(f1 flower, f2 flower) bool {
	if (c.color1 == f1.color && c.color2 == f2.color) ||
	(c.color1 == f2.color && c.color2 == f1.color) {
		return true
	}
	return false
}

func (fl flower) Points(prefs preferences, bed flowerbed, index int) int {
	var points int
	for _, neighbourIndex := range fl.neighbours {
		if neighbourIndex < index {
			continue
		} else if bed.flowers[neighbourIndex].color[0] == 'x' {
			continue
		}
		for _, p := range prefs.combinations {
			if p.Fits(fl, bed.flowers[neighbourIndex]) {
				points += p.points
			}
		}
	}
	return points
}

func (bed flowerbed) Points(prefs preferences) int {
	var points int
	for idx, flower := range bed.flowers {
		if flower.color[0] == 'x' {
			continue
		}
		points += flower.Points(prefs, bed, idx)
	}
	return points
}

func (bed flowerbed) Colors() []string {
	var buf []string = make([]string, 0, 7)
	var ctr int = 0
	for _, flower := range bed.flowers {
		if flower.color == "x" {
			ctr++
			buf = append(buf, "x" + strconv.Itoa(ctr))
		} else if !contains(buf, flower.color) {
			buf = append(buf, flower.color)
		}
	}
	return buf
}

func contains(slice []string, s string) bool {
	for _, x := range slice {
		if x == s {
			return true
		}
	}
	return false
}

func (prefs preferences) UniqueColors() []string {
	var buf []string
	for _, combination := range prefs.combinations {
		if !contains(buf, combination.color1) {
			buf = append(buf, combination.color1)
		}
		if !contains(buf, combination.color2) {
			buf = append(buf, combination.color2)
		}
	}
	return buf
}

func (bed flowerbed) String() string {
	var buf string
	for _, fl := range bed.flowers {
		buf += fl.color + " "
	}
	buf = buf[:len(buf)-1]
	return buf
}

type result struct {
	bed flowerbed
	points int
}

func findBestFlowerbed(prefs preferences) result {
	uniqueColors := prefs.UniqueColors()
	fmt.Println(uniqueColors)
	if (prefs.colors-len(uniqueColors) > 0) {
		uniqueColors = append(uniqueColors, "x")
	}
	res := result{}
	bed := createFlowerbed()

	findBestFlowerbedHelper(bed, &res, uniqueColors, prefs.colors, prefs, 8)
	// Platzhalter x mit Farben ersetzen
	for i, f := range res.bed.flowers {
		if f.color == "x" {
			for _, c := range availableColors {
				if !contains(uniqueColors, c) {
					res.bed.flowers[i].color = c
					uniqueColors = append(uniqueColors, c)
					break;
				}
			}
		}
	}
	return res
}

func findBestFlowerbedHelper(currentFlowerbed flowerbed, best *result, colorPool []string, 
							requiredColorsLeft int, prefs preferences, index int) {
	// Hier kann keine valide Kombination gefunden werden
	if requiredColorsLeft > index+1 {
		return
	}

	// Basisfall, mit bestem Blumenbeet vergleichen
	if index == -1 {
		pts := currentFlowerbed.Points(prefs)
		if pts > best.points {
			fmt.Println(currentFlowerbed, pts)
			best.bed = currentFlowerbed
			best.points = pts
		}
		return
	}

	// Optimierung: Spiegelungen ignorieren
	var idx int
	if index == 0 {
		for i, c := range colorPool {
			if currentFlowerbed.flowers[8].color == c {
				idx = i
				break
			}
		}
	} else if index == 1 {
		for i, c := range colorPool {
			if currentFlowerbed.flowers[2].color == c {
				idx = i
				break
			}
		}
	}

	for i := idx; i < len(colorPool); i++ {
		left := requiredColorsLeft
		if !contains(currentFlowerbed.Colors(), colorPool[i]) {
			left--
		}
		currentFlowerbed.flowers[index].color = colorPool[i]
		findBestFlowerbedHelper(currentFlowerbed, best, colorPool, left, prefs, index-1)
	}
}

func parseFile(filename string) preferences {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatalln("Can't open file", err.Error())
	}
	defer file.Close()

	prefs := preferences{}
	scanner := bufio.NewScanner(file)
	scanner.Scan()
	prefs.colors, _ = strconv.Atoi(scanner.Text()) // Anzahl der Farben
	scanner.Scan()
	n, _ := strconv.Atoi(scanner.Text()) // Anzahl der gegebenen Kombinationen
	for i := 0; i < n; i++ {
		scanner.Scan()
		buf := scanner.Text()
		x := strings.Split(buf, " ")
		if !contains(availableColors[:], x[0]) || !contains(availableColors[:], x[1]) {
			fmt.Errorf("Ignoring invalid combination: %v", buf)
			continue
		}
		var p combination
		p.color1 = x[0]
		p.color2 = x[1]
		p.points, _ = strconv.Atoi(x[2])

		prefs.combinations = append(prefs.combinations, p)
	}
	
	return prefs
}

func main() {
	if len(os.Args) < 2 {
		log.Fatalln("Usage:", os.Args[0], "filename")
	}

	startTime := time.Now()
	prefs := parseFile(os.Args[1])
	res := findBestFlowerbed(prefs)

	fmt.Println("Best:", res.bed, res.points)
	fmt.Println("Time elapsed:", time.Since(startTime))
}

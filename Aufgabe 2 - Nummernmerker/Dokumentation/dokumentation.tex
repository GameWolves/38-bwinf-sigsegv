\documentclass[backa4paper,10pt,ngerman]{scrartcl}
\usepackage{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage[a4paper,margin=2.5cm,footskip=0.5cm]{geometry}
\usepackage{qtree}
\usepackage{epsf}
\usepackage{float}

\usepackage{tikz,pgfplots}

\newcommand{\subsubsubsection}[1]{\paragraph{#1}\mbox{}\\}
\setcounter{secnumdepth}{4}
\setcounter{tocdepth}{4}

% Die nächsten drei Felder bitte anpassen:
\newcommand{\Aufgabe}{Aufgabe 2: Nummernmerker}
\newcommand{\TeamID}{00072}
\newcommand{\TeamName}{SIGSEGV}
\newcommand{\Namen}{Philipp Rempe}

% Kopf- und Fußzeilen
\usepackage{scrlayer-scrpage, lastpage}
\setkomafont{pageheadfoot}{\large\textrm}
\lohead{\Aufgabe}
\rohead{Team-ID: \TeamID}
\cfoot*{\thepage{} of \pageref{LastPage}}

% Position des Titels
\usepackage{titling}
\setlength{\droptitle}{-1.0cm}

% Für mathematische Befehle und Symbole
\usepackage{amsmath}
\usepackage{amssymb}

% Für Bilder
\usepackage{graphicx}

% Für Algorithmen
\usepackage{algpseudocode}

% Für Quelltext
\usepackage[formats]{listings}
\usepackage{color}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstset{
  keywordstyle=\color{blue},commentstyle=\color{mygreen},
  stringstyle=\color{mymauve},rulecolor=\color{black},
  basicstyle=\footnotesize\ttfamily,numberstyle=\tiny\color{mygray},
  captionpos=b, % sets the caption-position to bottom
  keepspaces=true, % keeps spaces in text
  numbers=left, numbersep=5pt, showspaces=false,showstringspaces=true,
  showtabs=false, stepnumber=2, tabsize=2, title=\lstname
}

% Diese beiden Pakete müssen zuletzt geladen werden
%\usepackage{hyperref} % Anklickbare Links im Dokument
\usepackage{cleveref}

% Daten für die Titelseite
\title{\textbf{\Huge\Aufgabe}}
\author{\LARGE Team-ID: \LARGE \TeamID \\\\
	    \LARGE Team-Name: \LARGE \TeamName \\\\
	    \LARGE Bearbeiter/-innen dieser Aufgabe: \\
	    \LARGE \Namen\\\\}
\date{\LARGE\today}

\begin{document}

\maketitle
\newpage
\tableofcontents
\newpage
\vspace{0.5cm}

\section{Lösungsidee}
Eine Zahl beliebiger Länge soll in beliebig viele Blöcke der Länge 2-4 unterteilt werden, wobei die Anzahl der Blöcke, die mit der Ziffer 0 beginnen, minimal ist. Demzufolge sind für einen Lösungsalgorithmus lediglich die Länge der Zahl und die Indizes der Nullen von Interesse.\\
Zuerst muss die einfachste Block-Kombination für die Eingabelänge gefunden werden, wobei 4-er-Blöcke die größte und 2-er Blöcke die niedrigste Priorität haben. Der eigentliche Algorithmus soll nun, mit der Startkombination und den Indizes der Nullen als Parameter, die günstigste Kombination errechnen. Hier bietet sich Rekursion und Backtracking an. Die Funktion findet zuerst die möglichen Veränderungen am aktuellen Block: Ein Block der Länge 3 kann beispielsweise um 1 wachsen (und somit auch einen nebenliegenden Block um 1 verkleinern) oder um 1 schrumpfen (und so einen nebenliegenden Block um 1 vergrößern). Diese werden nun ausprobiert, indem rekursiv am nächsten Block gearbeitet wird und danach mithilfe von Backtracking die anderen übriggebliebenen möglichen Veränderungen ausprobiert werden. Das Ziel hierbei ist die Grenzen zwischen den Blöcken so zu verschieben, dass möglichst wenige dieser Grenzen vor eine 0 fallen. So wird garantiert eine optimale Kombination gefunden.\\
Wenn aber wie im obigen Beispiel ein Block der Länge 3 um 1 geschrumpft wird und der nebenliegende Block, der vergrößert werden soll, bereits 4 Ziffern lang ist, muss dieser geteilt werden. Der Block der Länge 5 würde hier also mit 2 Blöcken [3, 2] ersetzt werden, damit das Längenlimit nicht überschritten wird.\\
Grundsätzlich gilt: ein Block kann maximal um 2 wachsen (bei einer Länge von 2) und maximal um 2 schrumpfen (bei einer Länge von 4).
\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth]{out.png}
  \caption{Rekursionsbaum bei einer Eingabe der Länge 10. Die Zahlen stellen die Länge der jeweiligen Blöcke dar}
  \label{fig:rekursionsbaum1}
\end{figure}
\subsection{Laufzeitanalyse}
Jeder Aufruf tätigt, wie in Abbildung \ref{fig:rekursionsbaum1} zu sehen, 0-3 weitere Rekursionsaufrufe, wobei die Laufzeit der jeweiligen Aufrufe von der Zusammensetzung der Eingabezahl, also der Position der Nullen, abhängt und somit nicht angegeben werden kann. Für die Anzahl der Aufrufe könnte man erst von einer asymptotischen oberen Schranke von $O(n) = 3^n$ ausgehen, wobei \emph{n} die Anzahl der Blöcke ist. Diese Annahme ist aber falsch, da sich beispielsweise eine Blockkombination [4, 3, 3] in [3, 2, 2, 3] aufteilen kann und dadurch die Anzahl der Blöcke $n$ um 1 erhöht und weitere Aufrufe durchführt. Wie man diese Aufteilungen in eine Mathematische Funktion miteinbeziehen kann ist mir unklar. Man kann jedoch sagen das Wachstum ist etwa $3^n$, also exponential.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.7\linewidth]{Funktionsaufrufe.png}
  \caption{Anzahl der Rekursionsaufrufe in Abhängigkeit der Eingabelänge}
  \label{fig:Funktionsaufrufe}
\end{figure}

\section{Umsetzung}
\subsection{findBestCombination}
Die Funktion \texttt{findBestCombination} ist ein benutzerfreundlicheres Interface zum eigentlichen Algorithmus \texttt{findBestCombinationHelper}.
\begin{lstlisting}[language=C++]
result findBestCombination(string nummer)
\end{lstlisting}
Als Parameter benötigt sie lediglich die Eingabenummer als \texttt{string}.
\begin{lstlisting}[language=C++]
vector<int> zeros = getzeros(nummer);
vector<int> blocks = splitIntoBlocks(nummer.length());
result buf;
buf.badBlocks = numOfBadBlocks(blocks, zeros);
buf.blocks = blocks;
findBestCombinationHelper(blocks, zeros, blocks.size()-1, buf);
return buf;
\end{lstlisting}
Sie findet mit \texttt{getzeros} die Indizes aller Nullen in der \texttt{nummer} und unterteilt sie mit \texttt{splitIntoBlocks} in eine ``einfache'' Kombination. \texttt{findBestCombinationHelper} hat ein \texttt{struct result} als Parameter und überschreibt diesen mit der gefundenen Kombination. \texttt{findBestCombination} gibt dieses dann zurück.

\subsection{findBestCombinationHelper}
Die rekursive Funktion des Algorithmus hat folgenden Header:
\begin{lstlisting}[language=C++]
void findBestCombinationHelper(vector<int>& blocks, const vector<int>& zeros,
                                                      int index, result& best)
\end{lstlisting}
In dem C++-Vektor \texttt{blocks} wird der aktuelle Stand der Blockkombination übergeben, zu Beginn ist diese die ``einfachste'' Kombination, also eine Kombination mit möglichst vielen 4-er, dann 3-er und mit niedrigster Priorität 2-er Blöcken. In dem Vektor \texttt{zeros} befinden sich die Indizes der Nullen. Index ist der Index des Elements in \texttt{blocks}, an dem die Funktion im aktuellen Aufruf arbeiten soll. Der Algorithmus geht \texttt{blocks} rückwärts durch, d.h. Index zeigt zu Beginn auf das letzte Element in \texttt{blocks}. In dem struct \texttt{best} wird aufrufübergreifend der Vektor der besten Blockkombination und deren Anzahl schlechter, d.h. mit Nullen beginnender, Blöcke gespeichert.
\subsubsection{Basisfall}
Wenn $index = 0$, dann ist der Algorithmus am ersten Block angelangt. Selbst wenn der erste Block mit einer 0 beginnt, kann keine Veränderung diesen Zustand verbessern. Deshalb ist der Algorithmus hier am Ende des Rekursionsbaumes angelangt und muss überprüfen, ob die Blockkombination in diesem Baumpfad optimaler ist als die in \texttt{best} gespeicherte, also entweder weniger ``schlechte'' Blöcke als diese hat oder gleich viele ``schlechte'' Blöcke, aber dafür eine geringere Anzahl an Blöcken insgesamt, hat. Wenn das der Fall ist wird \texttt{best.blocks} mit der aktuellen Kombination und \texttt{best.badBlocks} mit deren Anzahl ``schlechter'' Blöcke überschrieben.
\begin{lstlisting}[language=C++]
if (index == 0) {
  unsigned int newBadBlocks = numOfBadBlocks(blocks, zeros);
  if (newBadBlocks < best.badBlocks ||
     (newBadBlocks == best.badBlocks && blocks.size() < best.blocks.size())) {
    best.blocks = blocks;
    best.badBlocks = newBadBlocks;
  }
  return;
}
\end{lstlisting}
\subsubsection{Veränderungen}
Der Algoritmus findet zunächst alle möglichen relativen Veränderungen am Block und speichert diese im Vektor \texttt{possible}. Der Vektor \texttt{possible} wird standartmäßig mit einem Element {0} initialisiert, da ein Gleichbleiben des Blocks immer möglich ist. Eine negative relative Veränderung \emph{x} bedeutet eine Verschiebung um $\vert x \vert$ nach Links, eine positive eine Verschiebung um $x$ nach Rechts. In \texttt{pos} wird der index der ersten Ziffer des aktuellen Blocks in dem Eingabestring \texttt{nummer} gespeichert.
\begin{lstlisting}[language=C++]
vector<int> possible = {0}; // Standardmaessig kann immer um 0 verschoben werden
int pos = sumElements(0, index, blocks); // Aktueller Index in der Eingabezahl

// Nach Links
for (int i = -4+blocks[index]; i < 0; i++) {
  if (pos+i >= 2) {     // Verschiebung darf den ersten Block nicht zu klein machen
    possible.push_back(i);
  }
}
// Nach Rechts
for (int i = blocks[index]-2; i > 0; i--) {
  possible.push_back(i);
}
\end{lstlisting}
Danach wird durch \texttt{possible} iteriert, wobei \texttt{x} die aktuell auszuprobierende Veränderung ist. Zuerst wird aber in jedem Schleifendurchgang die Aktuelle Blockkombination \texttt{blocks} in \texttt{buf} kopiert und an dieser Kopie gearbeitet, damit im nächsten Schleifendurchgang \texttt{blocks} unverändert bleibt.\\

\paragraph{Verkleinerung (x < 0)}\mbox{}\\
Wenn $x < 0$, also eine Verschiebung nach Links vorgenommen werden soll, muss der aktuelle Block um \texttt{x} erhöht werden und im Gegenzug ein oder mehrere vorherige Blöcke um insgesamt \texttt{x} verkleinert werden. Wenn beispielsweise $x = -2$ und der vorherige Block \texttt{blocks[index-1]} nur 3 Lang ist, muss dieser und der ihm bevorstehende Block \texttt{[index-2]} jeweils um 1 verkleinert werden.
\begin{lstlisting}[language=C++]
if (x < 0) {
  for (int i = index-1; x < 0; x++) {
    while (buf[i] == 2) // Kann nicht weiter verkleinert werden, vorherigen Block nehmen
      i--;

    buf[i]--;
    buf[index]++;
  }

  findBestCombinationHelper(buf, zeros, index-1, best);
}
\end{lstlisting}
\paragraph{Vergrößerung (x > 0)}\mbox{}\\
Wenn $x < 0$, also eine Verschiebung nach Rechts durchgeführt werden soll, wird der aktuelle Block um \texttt{x} verringert und der ihm bevorstehende um \emph{x} erhöht. Wenn dabei der ihm bevorstehende Block größer als 4 wird, muss dieser aufgeteilt werden, wozu die Funktion \texttt{splitIntoBlocks} zuständig ist. Aus [5] wird also [3, 2] und aus [6] wird [3, 3]. In diesem Fall wird beim rekursiven Aufruf \texttt{index} nicht verkleinert, da \texttt{blocks} um 1 gewachsen ist.
\begin{lstlisting}[language=C++]
else if (x > 0) {
  buf[index] -= x;
  buf[index-1] += x;

  if (buf[index-1] > 4) {
    // Vorheriger Block muss aufgeteilt werden
    vector<int> splitted = splitIntoBlocks(buf[index-1]);
    buf[index-1] = splitted[0];
    buf.insert(buf.begin()+index, splitted.begin()+1, splitted.end());

    findBestCombinationHelper(buf, zeros, index, best);
  }
  else {
    findBestCombinationHelper(buf, zeros, index-1, best);
  }
}
\end{lstlisting}
\paragraph{Gleichbleiben (x == 0)}\mbox{}\\
Wenn $x = 0$ wird keine Veränderung durchgeführt und somit bloß die Funktion mit dem vorherigen Block als \texttt{index} aufgerufen.
\begin{lstlisting}[language=C++]
else {
  findBestCombinationHelper(buf, zeros, index-1, best);
}
\end{lstlisting}

\newpage
\newpage

\section{Beispiele}
\subsection{005480000005179734}
Der Helper-Funktion des Algorithmus wird die Start-Blockkombination in \texttt{blocks} übergeben, hier bei einer Länge von 18 also [4, 4, 4, 4, 2]. Der Startindex ist 4 also der Letzte Block mit der Länge 2. Hier ist die Startkombination mit 2 ``schlechten'' Blöcken jedoch schon die optimale Kombination, wodurch das Ergebnis [4, 4, 4, 4, 2] ist.\\
Beispiele 2-4 verhalten sich genauso, werden also nur kurz beschrieben.
\subsection{03495929533790154412660}
Ergebnis: 0349 5929 5337 9015 4412 660 mit einem ``schlechtem'' Block.
\subsection{5319974879022725607620179}
Ergebnis: 5319 9748 7902 2725 6076 201 79 mit 0 ``schlechten'' Blöcken.
\subsection{9088761051699482789038331267}
Ergebnis: 9088 7610 5169 9482 7890 3833 1267 mit 0 ``schlechten'' Blöcken.
\subsection{011000000011000100111111101011}
Bei einer Länge von 30 ist die Startkombinatoin hier [4, 4, 4, 4, 4, 4, 4, 2] mit 5 ``schlechten'' Blöcken. Zerteilt man die Nummer so in die Blöcke erhält man 0110 0000 0011 0001 0011 1111 1010 11. Wie hier zu sehen ist können der 4. und 5. Block ausgebessert werden. Die Funktion gibt [2, 4, 4, 4, 4, 4, 4, 4] zurück, also 01 1000 0000 1100 0100 1111 1110 1011, mit 3 ``schlechten'' Blöcken.\\

\section{Quellcode}
\lstinputlisting[language=C++]{rewrite.cpp}

\end{document}

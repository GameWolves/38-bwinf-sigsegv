#include <random>
#include <ctime>
#include <cmath>
#include <cctype>
#include <iostream>

#include "fahrt.hpp"

int main(int argc, char* argv[])
{
	std::srand(std::time(NULL));
	uintmax_t count = 0;

	toggleOutput();

	while(true) //Generiere die ganze Zeit Tests
	{
		Data data = Data();

		data.capacity = 50 + std::abs(std::rand() % 100);
		data.startAmount = data.capacity / 3 + std::rand() % (int)(data.capacity / 2);
		data.distance = 5000 + std::rand() % 10000;
		data.usage = 5 + std::rand() % 10;

		float maxReach = (float)data.capacity / (float)data.usage * 100;
		float initialReach = (float)data.startAmount / (float)data.usage * 100;

		int minGasStations = 1 + (int)std::ceil((data.distance - initialReach) / maxReach);

		data.gasStationCount = minGasStations * 2;//(2 + std::rand() % 5);

		float distance = 5 + std::rand() % ((int)std::floor(initialReach) - 5);

		data.stations.push_back(Gasstation((int)distance, std::rand() % 100 + 100));

		while(distance + maxReach < data.distance)
		{
			distance += (int)(maxReach / 2) + std::rand() % (int)(maxReach / 2);
			data.stations.push_back(Gasstation((int)distance, std::rand() % 100 + 100));
		}

		while(data.stations.size() < data.gasStationCount)
		{
			int position = std::rand() % (int)(data.distance - 1) + 1;
			data.stations.push_back(Gasstation((int)position, std::rand() % 100 + 100));
		}

		orderData(data);

		auto minimal = findMinimalConfig(data);
		auto config = optimizeConfig(data, minimal);

		std::cout << ++count << ". Test passed" << std::endl;
	}

	return 0;
}

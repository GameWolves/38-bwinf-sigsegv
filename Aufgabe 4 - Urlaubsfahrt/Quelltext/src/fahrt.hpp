#pragma once

#include <vector>
#include <map>
#include <string>

//Struct fuer eine Tankstelle
struct Gasstation
{
	float distance; //Position
	float price;	//Preis pro Liter

	Gasstation()
		: distance{0}, price{0}
	{}

	Gasstation(int distance, int price)
		: distance{(float)distance}, price{(float)price}
	{}
};

//Struct fuer Tankungen
struct Fuelling
{
	int index;		//Index in die Tankstellenliste
	float amount;	//Getankte Menge

	Fuelling()
		:index{0}, amount{0}
	{}

	Fuelling(int index, float amount)
		: index{index}, amount{amount}
	{}
};

//Struct fuer die Daten aus der Eingabedatei
struct Data
{
	float usage;						//Verbrauch in l/100km
	float capacity;						//Tankgroesse in l
	float startAmount;					//Tankfuellung in l
	float distance;						//Gesamtlaenge der Strecke in km
	int gasStationCount;				//Anzahl Tankstellen
	std::vector<Gasstation> stations;	//Tankstellen
};

//Struct fuer eine Konfiguration
struct Config
{
	std::vector<Fuelling> stations;		//Liste an Tankungen
	float totalPrice;					//Gesamtpreis
};

void toggleOutput();
Data getDataFromFile(std::string);
void orderData(Data&);

Config calculateConfig(Data&, std::vector<int>&);
Config optimizeConfig(Data&, std::vector<int>&);
void optimizeStation(Data&, std::vector<int>, Config&, int);
std::vector<int> findMinimalConfig(Data&);

bool isConfigurationLegal(Data&, std::vector<int>&);
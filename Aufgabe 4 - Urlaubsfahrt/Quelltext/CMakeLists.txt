cmake_minimum_required(VERSION 3.0)

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

if (MSVC)
string(CONCAT CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE}" " /GL")
string(CONCAT CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS}" " /LTCG")
endif (MSVC)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")

if (UNIX)
set(CMAKE_C_COMPILER gcc)
set(CMAKE_C_FLAGS -m32)
set(CMAKE_CXX_COMPILER g++)
set(CMAKE_CXX_FLAGS -m32)
endif (UNIX)

project(Urlaubsfahrt)

add_executable(Urlaubsfahrt
	src/main.cpp

	src/fahrt.hpp
	src/fahrt.cpp
)

if (UNIX)
set_target_properties (Urlaubsfahrt PROPERTIES COMPILE_OPTIONS "-m32" LINK_FLAGS "-m32")
endif (UNIX)


INSTALL(TARGETS Urlaubsfahrt DESTINATION "./bin")

project(Tester)

add_executable(Tester
	src/test.cpp

	src/fahrt.hpp
	src/fahrt.cpp
)

if (UNIX)
set_target_properties (Tester PROPERTIES COMPILE_OPTIONS "-m32" LINK_FLAGS "-m32")
endif (UNIX)

INSTALL(TARGETS Tester DESTINATION "./bin")

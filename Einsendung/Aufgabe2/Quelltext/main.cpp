#include <iostream>
#include <chrono>
#include <vector>
#include <algorithm>
#define _contains(a,b) (std::find(a.begin(), a.end(), b) != a.end())

using namespace std;
using namespace std::chrono;

string seperateNumberIntoBlocks(string nummer, const vector<int>& blocks)
{
	int x = nummer.length();
	for (unsigned int i = blocks.size()-1; i > 0; i--) {
		x -= blocks[i];
		if (x > 0) {
			nummer.insert(x, " ");
		}
	}
	return nummer;
}

vector<int> getzeros(string nummer)
{
	vector<int> res;
	for (unsigned int i = 0; i < nummer.length(); i++) {
		if (nummer[i] == '0')
			res.push_back(i);
	}
	return res;
}

vector<int> splitIntoBlocks(int length)
{
	vector<int> blocks;
	while (length > 0) {
		if (length == 5 || length == 3) {
			blocks.push_back(3);
			length -= 3;
		}
		else if (length >= 4) {
			blocks.push_back(4);
			length -= 4;
		}
		else {
			blocks.push_back(2);
			length -= 2;
		}
	}
	return blocks;
}

int numOfBadBlocks(const vector<int>& blocks, const vector<int>& zeros)
{
	int x = 0;
	int bad = 0;
	for (auto i : blocks) {
		if (_contains(zeros, x)) {
			bad++;
		}
		x += i;
	}
	return bad;
}

string vectorToString(vector<int> in)
{
	string buf;
	for (auto a : in) {
		buf += to_string(a) + " ";
	}
	if (!buf.empty())
		buf.pop_back();
	return buf;
}

int sumElements(int start, int end, const vector<int>& in)
{
	int sum = 0;
	for (int i = start; i < end; i++) {
		sum += in[i];
	}
	return sum;
}

int sumOfVector(const vector<int>& vec)
{
	int res = 0;
	for (auto a : vec) {
		res += a;
	}
	return res;
}

struct result {
	vector<int> blocks;
	unsigned int badBlocks;
};

vector<string> body;
void findBestCombinationHelper(vector<int>& blocks, const vector<int>& zeros, int index, result& best)
{
	// Base-case: Am Ende vom Rekursionsbaum angelangt, UEberpruefen ob
	// gefundene Kombination die Beste ist, wenn ja: speichern
	if (index == 0) {
		unsigned int newBadBlocks = numOfBadBlocks(blocks, zeros);
		if (newBadBlocks < best.badBlocks ||
			(newBadBlocks == best.badBlocks && blocks.size() < best.blocks.size())) {
			best.blocks = blocks;
			best.badBlocks = newBadBlocks;
		}
		return;
	}

	vector<int> possible = {0}; // Standardmaessig kann immer um 0 verschoben werden
	int pos = sumElements(0, index, blocks); // Aktueller Index in der Eingabezahl

	// Moegliche relative Veraenderungen nach Links finden
	for (int i = -4+blocks[index]; i < 0; i++) {
		if (pos+i >= 2) {	// Nicht nach Links gehen, wenn der erste Block dadurch zu klein wuerde
			possible.push_back(i);
		}
	}
	// Moegliche relative Veraenderungen nach Rechts finden
	for (int i = blocks[index]-2; i > 0; i--) {
		possible.push_back(i);
	}

	vector<int> buf;
	for (auto x : possible) {
		buf = blocks;	// Mit Deep-copy von Blocks arbeiten, damit Backtracking spaeter moeglich ist
		
		// Nach Links
		if (x < 0) {
			for (int i = index-1; x < 0; x++) {
				while (buf[i] == 2) // Kann nicht weiter verkleinert werden, vorherigen Block nehmen
					i--;
				buf[i]--;
				buf[index]++;
			}

			findBestCombinationHelper(buf, zeros, index-1, best);
		}

		// Nach Rechts
		else if (x > 0 && index > 0) {
			buf[index] -= x;
			buf[index-1] += x;

			if (buf[index-1] > 4) {
				// Vorheriger Block muss aufgeteilt werden
				vector<int> splitted = splitIntoBlocks(buf[index-1]);
				buf[index-1] = splitted[0];
				buf.insert(buf.begin()+index, splitted.begin()+1, splitted.end());

				findBestCombinationHelper(buf, zeros, index, best); // Vektor um 1 gewachsen
			}
			else {
				findBestCombinationHelper(buf, zeros, index-1, best);
			}
		}

		// Nicht veraendern
		else {
			findBestCombinationHelper(buf, zeros, index-1, best);
		}
	}
}

result findBestCombination(string nummer)
{
	vector<int> zeros = getzeros(nummer);
	vector<int> blocks = splitIntoBlocks(nummer.length());
	result buf;
	buf.badBlocks = numOfBadBlocks(blocks, zeros);
	buf.blocks = blocks;
	findBestCombinationHelper(blocks, zeros, blocks.size()-1, buf);
	return buf;
}

int main(int argc, char* argv[])
{
	if (argc < 2) {
		cout << "Usage: " << argv[0] << " number" << endl;
		return -1;
	}

	string nummer = argv[1];
	vector<int> base = splitIntoBlocks(nummer.length());
	cout << nummer << endl << seperateNumberIntoBlocks(nummer, base) << " (" << numOfBadBlocks(base, getzeros(nummer)) << ")" << endl;
	result res = findBestCombination(nummer);
	cout << seperateNumberIntoBlocks(nummer, res.blocks) << " (" << res.badBlocks << ")" << endl;
}

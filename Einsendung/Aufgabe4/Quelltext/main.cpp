#include <iostream>
#include <cstdio>
#include <chrono>
#include <string>

#include "fahrt.hpp"

int main(int argc, char* argv[])
{
	auto s = std::chrono::high_resolution_clock::now();

	if (argc != 2)
	{
		std::cerr << "Usage: " << argv[0] << " <Dateipfad>" << std::endl;
		exit(-1);
	}

	//Eingabedatei uebersetzen
	Data data = getDataFromFile(std::string(argv[1]));
	orderData(data);

	//Minimale Konfiguration finden
	std::cout << "Finde minimale Anzahl an Tankungen" << std::endl;
	auto minimal = findMinimalConfig(data);

	//Preis optimieren
	std::cout << "Optimiere Preis" << std::endl;
	auto config = optimizeConfig(data, minimal);
	std::cout << std::endl;

	//Ausgabe
	std::printf("Die guenstigste Konfiguration mit dem Preis %.2f Euro ist:\r\n", config.totalPrice / 100);
	for (auto& station : config.stations)
		std::cout << data.stations[station.index].distance << "km -> " << station.amount << "l" << std::endl;

	auto e = std::chrono::high_resolution_clock::now();
	std::cout << std::endl << "Verstrichene Zeit: " << std::chrono::duration_cast<std::chrono::milliseconds>(e - s).count() << "ms" << std::endl;
}
#include "fahrt.hpp"

#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <limits>

bool doOutput = true;

void toggleOutput()
{
	doOutput = !doOutput;
}

//Finde die billigste Konfiguration
Config optimizeConfig(Data& data, std::vector<int>& stations)
{
	//Anfaengliche beste Konfiguration mit maximalem Preis
	Config best = Config();
	best.totalPrice = std::numeric_limits<float>::max();

	//Binaerbaum von oberster Ebene aus optimieren
	optimizeStation(data, stations, best, stations.size() - 1);

	return best;
}

//Optimisiert einen Subbaum des ganzen Binaerbaums
void optimizeStation(Data& data, std::vector<int> current, Config& best, int index)
{
	//Alle vorangehenden Tankstellen inklusive der derzeitigen
	for (int i = current[index]; i >= 0; i--)
	{
		//Konfiguration aendern und ueberpruefen
		current[index] = i;
		if (!isConfigurationLegal(data, current))
			return;

		if (index > 0) //Solange nicht die tiefste Baumebene erreicht ist
			optimizeStation(data, current, best, index - 1); //Tieferen Subbaum ueberpruefen
		else
		{
			//Neue Konfiguration berechnen und testen ob diese billiger ist
			Config now = calculateConfig(data, current);
			if (now.totalPrice < best.totalPrice)
			{
				if (doOutput)
					printf("%.2f Euro\n", now.totalPrice / 100);

				//Referenz auf beste Konfiguration erneuern
				best = now;
			}
		}
	}
}

//Minimale Konfiguration finden
std::vector<int> findMinimalConfig(Data& data)
{
	//Derzeitige Tankstelle (-1: Startpunkt; data.stations.size(): Ziel)
	int current = -1;
	std::vector<int> stations;

	//Solange das Ziel nicht erreicht wurde
	while(current != data.stations.size())
	{
		float range = 0;
		float position = 0;

		if (current == -1)
			//Anfaengliche Reichweite berechnen
			range = data.startAmount / data.usage * 100;
		else
		{
			//Volle Reichweite und neue Posiion berechnen
			position = data.stations[current].distance;
			range = data.capacity / data.usage * 100;
		}

		//Alle darauffolgenden Tankstellen
		for (int i = current + 1; i <= data.stations.size(); i++)
		{
			//Position des derzeitigen Ziels
			float destination = 0;

			if (i == data.stations.size())
				destination = data.distance; //Position des Ziels
			else
				destination = data.stations[i].distance; //Position der naechsten Tankstelle

			//Ist das naechste Ziel ausser Reichweite
			if (destination - position > range)
			{
				//Ist somit kein weiteres Ziel in Reichweite
				if (i == 0 || (stations.size() > 0 && stations[stations.size() - 1] == i - 1))
				{
					//Keine Loesung
					std::cerr << "Cannot reach gas station! Exiting!" << std::endl;
					exit(-1);
				}

				//Die vorherige Tankstelle nehmen (die noch in Reichweite war)
				current = i - 1;
				stations.push_back(i - 1);
				break;
			}
			else if (i == data.stations.size()) //Ziel in Reichweite
			{
				//Zum Ziel fahren
				current = i;
				break;
			}
		}
	}

	return stations;
}

//Berechnet aus einer Liste gewaehlter Tankstellen die Konfiguration
Config calculateConfig(Data& data, std::vector<int>& stations)
{
	Config config = Config();

	//Derzeitiger Tankstand
	float currentFuel = data.startAmount;

	//Für jede Tankstelle
	for (int i = 0; i < stations.size(); i++)
	{
		//Gefahrene Distanz berechnen
		float distanceDriven = data.stations[stations[i]].distance;
		if (i > 0) //Bereits gefahrene Strecke abziehen
			distanceDriven -= data.stations[stations[i - 1]].distance;
		//Benutzten Tank abziehen
		currentFuel -= distanceDriven / 100 * data.usage;

		//Zu fahrende Strecke berechnen
		float distanceToDrive = 0;
		if (i == stations.size() - 1) //Ist das Ziel zu erreichen
			distanceToDrive = data.distance;
		else
			distanceToDrive = data.stations[stations[i + 1]].distance;
		//Bereits gefahrene Strecke abziehen
		distanceToDrive -= data.stations[stations[i]].distance;

		//Zu kaufende Benzinmenge berechnen
		float fuelToBuy = 0;
		if (i == stations.size() - 1) //Ist das Ziel zu erreichen
			fuelToBuy = distanceToDrive / 100 * data.usage;
		else
		{
			//Ist die narchste Tankstelle teurer als die jetzige
			if (data.stations[stations[i + 1]].price > data.stations[stations[i]].price)
				fuelToBuy = data.capacity; //Volltanken
			else
				fuelToBuy = distanceToDrive / 100 * data.usage; //So viel wie noetig tanken
		}
		//Deerzeitigen Tankstand abziehen
		fuelToBuy -= currentFuel;

		//Konfiguration und Tankstand updaten
		config.stations.push_back(Fuelling(stations[i], fuelToBuy));
		config.totalPrice += fuelToBuy * data.stations[stations[i]].price;

		currentFuel += fuelToBuy;
	}

	return config;
}

//Ueberprueft ob eine Konfiguration moeglich ist
bool isConfigurationLegal(Data& data, std::vector<int>& stations)
{
	//Maximale Reichweite
	float maxDistance = data.capacity / data.usage * 100;

	//Fuer alle Tankstellen
	for (int i = 0; i < stations.size(); i++)
	{
		//Erster Punkt
		float distance1 = data.stations[stations[i]].distance;
		float distance2;

		//Zweiter Punkt; Entweder Ziel oder naechste Tankstelle
		if (i == stations.size() - 1)
			distance2 = data.distance;
		else
			distance2 = data.stations[stations[i + 1]].distance;

		//Zu fahrende Strecke laenger als Reichweite
		if (distance2 - distance1 > maxDistance)
			return false;
	}

	//Anfaengliche Reichweite berechnen
	float initialDistance = data.startAmount / data.usage * 100;
	//Ist die erste Tankstelle ausser Reichweite
	if (data.stations[stations[0]].distance > initialDistance)
		return false;
	
	return true;
}

//Ordnet die Tankstellen des Data-Structs
void orderData(Data& data)
{
	//Tankstellen anhand Distanz sortieren
	std::sort(
		data.stations.begin(),
		data.stations.end(),
		[] (Gasstation& a, Gasstation& b) { return a.distance < b.distance; }
	);
}

//Uebersetzt die Eingabedatei in ein Data-Struct
Data getDataFromFile(std::string filepath)
{
	std::cout << "Lese Datei" << std::endl;

	std::string line;
	std::ifstream fstream(filepath);

	Data data = Data();

	if (fstream.is_open())
	{
		//Lambda welches Errorhandling mit einbezieht
		auto getline = [](std::ifstream& f, std::string& l) {
			if (!std::getline(f, l))
			{
				std::cerr << "Falsche oder kaputte Datei!" << std::endl;
				f.close();
				exit(-1);
			}
		};

		getline(fstream, line);
		data.usage = std::stoi(line);

		getline(fstream, line);
		data.capacity = std::stoi(line);

		getline(fstream, line);
		data.startAmount = std::stoi(line);

		getline(fstream, line);
		data.distance = std::stoi(line);

		getline(fstream, line);
		data.gasStationCount = std::stoi(line);

		std::cout << data.usage << "  " << data.capacity << "  " << data.startAmount << "  " << data.distance << "  " << data.gasStationCount << std::endl;

		data.stations.resize(data.gasStationCount);
		for (int i = 0; i < data.gasStationCount; i++)
		{
			getline(fstream, line);

			int distance = -1;
			int price;

			std::istringstream iss(line);
			std::string tmp;

			//Zeile an Leerzeichen splitten
			while(std::getline(iss, tmp, ' '))
			{
				if (tmp.compare("") != 0)
				{
					if (distance == -1)
						distance = std::stoi(tmp);
					else
						price = std::stoi(tmp);
				}
			}

			std::cout << distance << " -> " << price << std::endl;

			data.stations[i] = Gasstation(distance, price);
		}
	}
	else
	{
		std::cerr << "Kann die Datei nicht öffnen!" << std::endl;
		exit(-1);
	}

	fstream.close();

	std::cout << "Datei gelesen." << std::endl << std::endl;
	return data;
}
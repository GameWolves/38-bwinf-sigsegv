#include <iostream>
#include <chrono>
#include <string>

#include "visual.hpp"
#include "rominos.hpp"

int main(int argc, char* argv[])
{
	bool drawSets = false;
	bool forceDraw = false;
	bool disableDraw = false;
	int n;

	if (argc < 3)
	{
		std::cerr << "Usage: " << argv[0] << " -n zahl" << std::endl;
		exit(-1);
	}

	std::string previous = "";
	for (int i = 1; i < argc; i++)
	{
		std::string current = std::string(argv[i]);
		
		if (current.find('-') != std::string::npos)
		{
			if (previous == "-n")
			{
				std::cerr << "Fehlende Zahl hinter -n Argument " << std::endl;
				exit(-1);
			}
			else if (current == "-force")
				forceDraw = true;
			else if (current == "-none")
				disableDraw = true;
			else if (current == "-sets")
				drawSets = true;
			else if (current != "-n")
			{
				std::cerr << "Unbekanntes Argument: " << current << std::endl;
				exit(-1);
			}
			
		}
		else
		{
			if (previous == "-n")
			{
				n = std::stoi(current);
				if (n < 2)
				{
					std::cerr << "n muss mindestens 2 sein" << std::endl;
					exit(-1);
				}
			}
			else
			{
				std::cerr << "Falsches Argument and dieser Stelle: " << current << std::endl;
				exit(-1);
			}
		}

		previous = current;
	}

	if (forceDraw && disableDraw)
	{
		std::cerr << "-force und -none koennen nicht gleichzeitig definiert werden" << std::endl;
		exit(-1);
	}


	auto s = std::chrono::high_resolution_clock::now();

	auto sets = calculateRominos(n);

	auto e = std::chrono::high_resolution_clock::now();
	std::cout << "Verstrichene Zeit fuer " << n << "-Rominos: " << std::chrono::duration_cast<std::chrono::milliseconds>(e - s).count() << "ms" << std::endl;


	std::cout << "Es gibt " << sets.size() << " " << n << "-Rominos." << std::endl;

	if (forceDraw || (!disableDraw && n < 8))
		createVisualization(sets, drawSets);
}
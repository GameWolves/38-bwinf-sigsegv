#pragma once

#include <vector>
#include <unordered_map>
#include <functional>

#pragma region Point_Struct

// Stellt einen 2D-Punkt mit x- und y-Koordinate dar
struct Point
{
	int x;
	int y;

	Point add(int x, int y);
	Point rotate90();
	Point mirrorXY();

	bool operator==(const Point& b) const;

	Point operator+(const Point& b);
	Point operator-(const Point& b);
};

#pragma region UnorderedMapHelperStruct

// Dient nur dazu dass die UnorderedMap Point Structs als Key nehmen kann
struct PointHasher
{
	std::size_t operator()(const Point& k) const
	{
		using std::hash;

		return (hash<int>()(k.x)) ^ (hash<int>()(k.y) << 1);
	}
};
#pragma endregion
#pragma endregion

#pragma region Romino_Struct

// Stellt einen Romino dar
struct Romino
{
	// Liste aller Positionen der Bloecke im Romino
	std::vector<Point> positions;

	// Derzeitige Blickrichtung
	Point currentDirection;

	// Erstellt einen Standardromino
	Romino()
	{
		positions = {{0, 0}, {1, 1}};
		currentDirection = {1, 1};
	}

	bool isIllegalCorner(Point& p);		// Testet ob ein Block im Romino in einer L-Formation mit anliegenden Blöcken ist.
	bool isLegal();						// Testet für jeden Block im Romino ob dieser in einer L-Formation mit anliegenden Blöcken ist.

	bool operator==(Romino& b);	// Testet ob ein Romino laut Definition gleich einem anderen Romino ist.
};
#pragma endregion

#pragma region Algorithm
std::vector<std::vector<Romino>> identifySets(std::vector<Romino> rominos);				// Fässt alle gleichen Rominos zu Sets zusammen.

void extendRomino(std::vector<Romino>& rominos, Romino romino, int direction);			// Verlängert einen Romino um die angegebene Richtung und setzt ihn falls er "legal" ist in die Liste.

std::vector<std::vector<Romino>> calculateRominos(int n);								// Berechnet alle n-Rominos (Anfang des Algorithmus).
#pragma endregion
#include "visual.hpp"

#include "rominos.hpp"

#include <SFML/Graphics.hpp>
#include <iostream>

constexpr int BlockWidth = 10;
constexpr int Padding = 5;
int MaxRominosPerRow = -1;
int RominoWidth = -1;
int n = -1;

int scroll = 0;

////////// Static function definitions //////////
// Rendered alle Rominos.
static void drawUniqueRominos(sf::RenderWindow& window, std::vector<std::vector<Romino>>& rominoSets);

// Rendered alle Rominosets untereinander.
static void drawRominoSets(sf::RenderWindow& window, std::vector<std::vector<Romino>>& rominoSets);

// Rendere einen Romino an der Position
static void drawRomino(sf::RenderWindow& window, int x, int y, Romino romino);

// Berechne die tatsaechliche Position
static sf::Vector2f calculateOffset(int iX, int iY);

////////// Static function implementations //////////
// Rendered alle Rominos
static void drawUniqueRominos(sf::RenderWindow& window, std::vector<std::vector<Romino>>& rominoSets)
{
	int iX, iY;
	for (int i = 0; i < (int)rominoSets.size(); i++)
	{
		iX = i % MaxRominosPerRow;
		iY = i / MaxRominosPerRow;
		drawRomino(window, iX, iY, rominoSets[i][0]);
	}
}

// Rendered alle Rominosets untereinander
static void drawRominoSets(sf::RenderWindow& window, std::vector<std::vector<Romino>>& rominoSets)
{
	int iY = 0;
	for (int i = 0; i < (int)rominoSets.size(); i++, iY++)
	{
		for (int iX = 0; iX < (int)rominoSets[i].size(); iX++)
		{
			if (iX % MaxRominosPerRow == 0 && iX != 0)
				iY++;
			drawRomino(window, iX % MaxRominosPerRow, iY, rominoSets[i][iX]);
		}
		sf::Vector2f position = calculateOffset(0, iY);
		position.y += (RominoWidth + Padding) / 2;
		position.x = 0;
		
		sf::RectangleShape line = sf::RectangleShape();
		line.setSize(sf::Vector2f(window.getSize().x, Padding / 2));
		line.setFillColor(sf::Color::Black);
		line.setPosition(position);
		window.draw(line);
	}
}

// Rendere einen Romino an der Position
static void drawRomino(sf::RenderWindow& window, int iX, int iY, Romino romino)
{
	sf::Vector2f offset = calculateOffset(iX, iY);

	sf::RectangleShape rect = sf::RectangleShape();
	rect.setSize(sf::Vector2f((float)BlockWidth, (float)BlockWidth));
	rect.setFillColor(sf::Color::Blue);

	for (auto pos : romino.positions)
	{
		rect.setPosition(offset + sf::Vector2f((float)pos.x * BlockWidth, (float)pos.y * BlockWidth));
		window.draw(rect);
	}
}

// Berechne die tatsaechliche Position
static sf::Vector2f calculateOffset(int iX, int iY)
{
	int x = Padding + iX * (RominoWidth + Padding) + (n - 1) * BlockWidth;
	int y = Padding + iY * (RominoWidth + Padding) + (n - 1) * BlockWidth + scroll;
	return {(float)x, (float)y};
}

void createVisualization(std::vector<std::vector<Romino>>& rominoSets, bool drawSets)
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "Romino Visualisierung");

	n = rominoSets[0][0].positions.size();
	RominoWidth = (BlockWidth * (n * 2 - 1));
	MaxRominosPerRow = (window.getSize().x - Padding * 2) / (RominoWidth + BlockWidth);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch(event.type)
			{
				case sf::Event::Closed:
					window.close();
					break;
				
				case sf::Event::MouseWheelScrolled:
					scroll += event.mouseWheelScroll.delta * 20;
					break;

				case sf::Event::KeyPressed:
					switch(event.key.code)
					{
						case sf::Keyboard::Up:
							scroll += 20;
							break;
						
						case sf::Keyboard::Down:
							scroll -= 20;
							break;
					}
					break;
			}
		}

		window.clear(sf::Color::White);
		
		if (drawSets)
			drawRominoSets(window, rominoSets);
		else
			drawUniqueRominos(window, rominoSets);

		window.display();
	}
}
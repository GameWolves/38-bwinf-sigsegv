#include "rominos.hpp"
#include <cmath>
#include <algorithm>
#include <iostream>

// Konstanten:
#pragma region Constants
// Berechnet die neue Blickrichtung aus der alten und der Bewegungsrichtung
std::unordered_map<Point, std::vector<Point>, PointHasher> DirectionDecoder = {
	{{ 0,  1}, {{ 0,  0}, { 1,  1}, { 0,  1}, {-1,  1}, { 0,  0}}},
	{{ 1,  1}, {{ 1, -1}, { 1,  0}, { 1,  1}, { 0,  1}, {-1,  1}}},
	{{ 1,  0}, {{ 0,  0}, { 1, -1}, { 1,  0}, { 1,  1}, { 0,  0}}},
	{{ 1, -1}, {{-1, -1}, { 0, -1}, { 1, -1}, { 1,  0}, { 1,  1}}},
	{{ 0, -1}, {{ 0,  0}, {-1, -1}, { 0, -1}, { 1, -1}, { 0,  0}}},
	{{-1, -1}, {{-1,  1}, {-1,  0}, {-1, -1}, { 0, -1}, { 1, -1}}},
	{{-1,  0}, {{ 0,  0}, {-1,  1}, {-1,  0}, {-1, -1}, { 0,  0}}},
	{{-1,  1}, {{ 1,  1}, { 0,  1}, {-1,  1}, {-1,  0}, {-1, -1}}}
};
#pragma endregion

// Implementation des Point Structs
#pragma region Point_Struct_Implementation

bool Point::operator==(const Point& b) const
{
	return this->x == b.x && this->y == b.y;
}

Point Point::add(int x, int y)
{
	return {this->x + x, this->y + y};
}

Point Point::rotate90()
{
	return {y, -x};
}

Point Point::mirrorXY()
{
	return {y, x};
}

Point Point::operator+(const Point& b)
{
	return {this->x + b.x, this->y + b.y};
}

Point Point::operator-(const Point& b)
{
	return {this->x - b.x, this->y - b.y};
}

#pragma region UnorderedMapHelperFunction

bool operator<(const Point& a, const Point& b)
{
	return (a.x + a.y) < (b.x + b.y);
}

#pragma endregion

#pragma endregion

// Implementation des Romino Structs
#pragma region Romino_Struct_Implementation

// Gibt aus ob zwei Rominos gleich sind.
bool Romino::operator==(Romino& other)
{
	auto begin = other.positions.begin();
	auto end = other.positions.end();

	bool equal[8];

	// Teste fuer jeden moeglichen Ursprung im ersten Romino ...
	for (auto& origin : this->positions)
	{
		for (auto& b : equal) b = true;

		for (auto& block : this->positions)	// ... ob jeder Block in ersten Romino ...
		{
			Point normal = block - origin;		// ... relativ zum neuen Ursprung ...
			Point mirror = normal.mirrorXY();

			// ... im anderen Romino enthalten ist.
			if (equal[0] && std::find(begin, end, normal) == end) 		equal[0] = false;

			// ... an der X-Y Diagonalen gespiegelt im anderen Romino enthalten ist.
			if (equal[1] && std::find(begin, end, mirror) == end) 	equal[1] = false;

			normal = normal.rotate90();
			mirror = mirror.rotate90();

			// ... um 90 Grad gedreht im anderen Romino enthalten ist.
			if (equal[2] && std::find(begin, end, normal) == end) 		equal[2] = false;

			// ... gespiegelt um 90 Grad gedreht im anderen Romino enthalten ist.
			if (equal[3] && std::find(begin, end, mirror) == end) 	equal[3] = false;

			normal = normal.rotate90();
			mirror = mirror.rotate90();
			if (equal[4] && std::find(begin, end, normal) == end) 		equal[4] = false; // etc.
			if (equal[5] && std::find(begin, end, mirror) == end) 	equal[5] = false;

			normal = normal.rotate90();
			mirror = mirror.rotate90();
			if (equal[6] && std::find(begin, end, normal) == end) 		equal[6] = false;
			if (equal[7] && std::find(begin, end, mirror) == end) 	equal[7] = false;

			// Sind alle Faelle false, breche aus dem Loop.
			bool dontBreak = false;
			for (auto& b : equal) dontBreak |= b;
			if (!dontBreak) break;
		}

		//Wenn einer der Faelle zutrifft sind die Rominos gleich.
		for (auto& b : equal)
			if (b)
				return true;
	}

	return false;
}

// Testet ob ein Block im Romino in einer L-Formation mit anliegenden Blöcken ist.
bool Romino::isIllegalCorner(Point& p)
{
	auto begin = this->positions.begin();
	auto end = this->positions.end();

	// Testet ob der Block darüber sich im Romino befindet.
	bool up = std::find(begin, end, p.add(0, -1)) != end;

	// Testet ob der Block darunter sich im Romino befindet.
	bool down = std::find(begin, end, p.add(0, 1)) != end;

	bool left = std::find(begin, end, p.add(-1, 0)) != end;	// etc.
	bool right = std::find(begin, end, p.add(1, 0)) != end;

	// Returned ob ein Horizontaler und ein Vertikaler Block angliegen -> L-Formation
	return (up || down) && (left || right);
}

// Testet für jeden Block im Romino 
// ob dieser in einer L-Formation mit anliegenden Blöcken ist.
bool Romino::isLegal()
{
	// Wendet die isIllegalCorner Funktion auf jeden Block im Romino an
	// und returned ob keiner "illegal" ist.
	return std::none_of(
		this->positions.begin(),
		this->positions.end(),
		[this] (Point& p) { return this->isIllegalCorner(p); }
	);
}

#pragma endregion

// Funktionen des Algorithmus
#pragma region Algorithm

// Faesst alle gleichen Rominos zu Sets zusammen
std::vector<std::vector<Romino>> identifySets(std::vector<Romino> rominos)
{
	std::vector<std::vector<Romino>> rominoSets;

	// Iteriere durch alle Rominos
	for (int i = 0; i < rominos.size(); i++)
	{
		if (!(i % 100))
			std::cout << i << "/" << rominos.size() << " Rominos ueberprueft." << std::endl;

		bool found = false;

		// Iteriere durch die derzeitigen Sets
		for (auto& set : rominoSets)
		{
			// Vergleiche einen Rominos aus dem Set mit dem derzeitigen
			if (rominos[i] == set[0])
			{
				found = true;

				// Fuege den Romino dem Set hinzu und gehe zum naechsten
				set.push_back(rominos[i]);
				break;
			}
		}

		// Wurde kein gleiches Rominoset gefunden?
		if (!found)
			// Erstelle ein neues Set mit diesem Romino
			rominoSets.push_back({ rominos[i] });
	}

	std::cout << rominoSets.size() << " Sets gefunden." << std::endl;
	return rominoSets;
}

// Verlaengert einen Romino um die angegebene Richtung und
// setzt ihn falls er "legal" ist in die Liste.
void extendRomino(std::vector<Romino>& rominos, Romino r, int direction)
{
	// Nehme die neue Blickrichtung aus dem DirectionDecoder.
	r.currentDirection = DirectionDecoder[r.currentDirection][direction];

	// Berechne den neuen Block.
	Point coord = r.positions[r.positions.size() - 1] + r.currentDirection;

	// Ist dieser schon im Romino enthalten? Breche ab.
	if (std::find(r.positions.begin(), r.positions.end(), coord) != r.positions.end())
		return;

	// Fuege den Block zum Romino hinzu.
	r.positions.push_back(coord);

	// Ist der Romino somit nichtmehr legal? Breche ab.
	if (!r.isLegal())
		return;

	// Fuege den neuen Romino zur Liste hinzu.
	rominos.push_back(r);
}

// Berechnet alle n-Rominos (Anfang des Algorithmus)
std::vector<std::vector<Romino>> calculateRominos(int n)
{
	std::vector<Romino> oldRominos = { Romino() };
	std::vector<Romino> newRominos;

	// Rominos beginnen mit zwei Bloecken, also den Counter - 2 nehmen
	n -= 2;

	// Waehrend die gewuenschte Laenge noch nicht erreicht ist
	while (n-- > 0)
	{
		// Loesche die neue Liste
		newRominos.clear();

		// Fuer jeden Romino alle moeglichen Richtungen durchgehen.
		for (auto romino : oldRominos)
			for (int direction = 0; direction < 5; direction++)
				// Romino verlaengern und evtl. an die Liste anhaengen
				extendRomino(newRominos, romino, direction); 

		// Kopiere die neue Liste in die alte
		oldRominos = newRominos;
		
		if (n > 0)
			std::cout << "Noch " << n << " Iterationen" << std::endl;
		else
			std::cout << "Rominos generiert." << std::endl;
	}

	std::cout << "Zu vergleichende Rominos: " << oldRominos.size() << std::endl;

	std::cout << std::endl << "Identifiziere Sets:" << std::endl;

	// Gleiche Rominos zusammenpacken
	return identifySets(oldRominos);
}
#pragma endregion
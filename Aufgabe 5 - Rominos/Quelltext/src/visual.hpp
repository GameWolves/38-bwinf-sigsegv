#pragma once

#include <vector>

#include "rominos.hpp"

// Kreiert die Visualisation der Rominos
// Wenn drawSets durch das Kommandozeilenargument -sets gesetzt ist, werden alle Sets untereinander gerendert.
void createVisualization(std::vector<std::vector<Romino>>& rominoSets, bool drawSets);